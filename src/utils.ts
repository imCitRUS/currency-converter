import axios from "axios"
import locales from "@/data/locales.json"

import type currencies from "@/data/currencies.json"

export async function getCurrencyRate(from: string, to: string) {
    const { data } = await axios.get("https://min-api.cryptocompare.com/data/pricemulti", {
        params: {
            fsyms: from,
            tsyms: to
        }
    })
    const obj = data[Object.keys(data)[0]]
    return obj[Object.keys(obj)[0]]
}

export function getBrowserCurrency(): keyof typeof currencies {
    const parts = navigator.language.toLowerCase().split("-")
    const localeList = Object.keys(locales)

    if (parts.length == 2) {
        return localeList.includes(parts[1]) ? (locales as any)[parts[1]] : "USD"
    } else {
        return localeList.includes(parts[0]) ? (locales as any)[parts[0]] : "USD"
    }
}
